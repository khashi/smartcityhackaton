﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ArmanShahr.Models
{
    public class Point
    {
        public long Id { get; set; }
        public ICollection<Report> Type { get; set; }
    }
}

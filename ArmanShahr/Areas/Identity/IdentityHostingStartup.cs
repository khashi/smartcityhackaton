﻿using System;
using ArmanShahr.Models;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.UI;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

[assembly: HostingStartup(typeof(ArmanShahr.Areas.Identity.IdentityHostingStartup))]
namespace ArmanShahr.Areas.Identity
{
    public class IdentityHostingStartup : IHostingStartup
    {
        public void Configure(IWebHostBuilder builder)
        {
            builder.ConfigureServices((context, services) => {
                services.AddDbContext<ArmanShahrContext>(options =>
                    options.UseSqlServer(
                        context.Configuration.GetConnectionString("ArmanShahrContextConnection")));

                services.AddDefaultIdentity<IdentityUser>()
                    .AddEntityFrameworkStores<ArmanShahrContext>();
            });
        }
    }
}
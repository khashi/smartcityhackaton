﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;

namespace ArmanShahr.Areas.Identity.Data
{
    public class ApplicationUser:IdentityUser
    {
        public string Name { get; set; }
        public string Family  { get; set; }
        public string Address { get; set; }
        public string NationalCode { get; set; }
    }
}

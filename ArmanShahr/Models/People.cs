﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ArmanShahr.Models
{
    public class People
    {
        public string Id { get; set; }
        public ICollection<Report> Report { get; set; }
    }
}

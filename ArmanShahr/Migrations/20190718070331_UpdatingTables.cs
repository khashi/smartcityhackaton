﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace ArmanShahr.Migrations
{
    public partial class UpdatingTables : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Description",
                table: "Reports",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "PeopleId",
                table: "Reports",
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "PointId",
                table: "Reports",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Reports_PeopleId",
                table: "Reports",
                column: "PeopleId");

            migrationBuilder.CreateIndex(
                name: "IX_Reports_PointId",
                table: "Reports",
                column: "PointId");

            migrationBuilder.AddForeignKey(
                name: "FK_Reports_Peoples_PeopleId",
                table: "Reports",
                column: "PeopleId",
                principalTable: "Peoples",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Reports_Points_PointId",
                table: "Reports",
                column: "PointId",
                principalTable: "Points",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Reports_Peoples_PeopleId",
                table: "Reports");

            migrationBuilder.DropForeignKey(
                name: "FK_Reports_Points_PointId",
                table: "Reports");

            migrationBuilder.DropIndex(
                name: "IX_Reports_PeopleId",
                table: "Reports");

            migrationBuilder.DropIndex(
                name: "IX_Reports_PointId",
                table: "Reports");

            migrationBuilder.DropColumn(
                name: "Description",
                table: "Reports");

            migrationBuilder.DropColumn(
                name: "PeopleId",
                table: "Reports");

            migrationBuilder.DropColumn(
                name: "PointId",
                table: "Reports");
        }
    }
}

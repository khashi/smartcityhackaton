﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ArmanShahr.Models
{
    public class Report
    {
        public long Id { get; set; }
        public string Description { get; set; }
        public People People{ get; set; }
        public Point Point { get; set; }
    }
}
